## WEb LIstening TAbles (WELITA)

Version 0.6.1

Generation of simple web tables, for comparison of audio samples and
listening tests (comparative (CMOS) and absolute scoring (MOS)).

#### Current features
* --wav_resample: Resample the waveform to the given sampling rate.
* --wav_normalize: Normalize the waveform to the given dB level.
* --wav_aligndelay: Align the samples on reference samples.
* Generation of comparison tables.
* Generation of Comparative Mean Opinion Score (CMOS) listening tests.
* Generation of Mean Opinion Score (MOS) listening tests.
* The tests return the time when a sound has been played for the very first
  time (first onplay event).
* The tests return the last time of the selected grade (last onclick event
  among the possible choices).

A demonstration webpage is available [here](http://gillesdegottex.eu/LT/welita/)

#### Dependencies
In order to run this code, you need the following modules/packages:
* From your favorite package system: python-numpy python-scipy cython libsndfile1-dev sox
* pysndfile: for reading files in many various formats, which can be installed through pip


#### Compilation
In the root directory, simply run:
```
$ make
```

#### Documentation
A few information is given in the header of each function.
You should mainly rely on some common usage that can be found in the test suite: `tests/test_smoke.py:TestBase::test_smoke_listtable_html_wav`


#### Legal
Copyright(C) 2016 Engineering Department, University of Cambridge, UK.

This source code is under the Apache License 2.0.
See LICENSE.md or <https://www.apache.org/licenses/LICENSE-2.0>

Author: Gilles Degottex <gad27@cam.ac.uk>

#### Disclaimer

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
ALSO, THE COPYRIGHT HOLDERS AND CONTRIBUTORS DO NOT TAKE ANY LEGAL
RESPONSIBILITY REGARDING THE IMPLEMENTATIONS OF THE PROCESSING TECHNIQUES
OR ALGORITHMS (E.G. CONSEQUENCES OF BUGS OR ERRONEOUS IMPLEMENTATIONS).
