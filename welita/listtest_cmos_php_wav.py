#!/usr/bin/env python

'''
COPYRIGHT(C) 2015 Machine Intelligence Lab, Engineering Dept, Univ Cambridge, UK
COPYRIGHT(C) 2018 ObEN, Inc. Pasadena, CA/US.

LICENSE
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
     http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

AUTHOR
    Gilles Degottex <gad27@cam.ac.uk> <gilles@oben.com>
'''

import sys
import argparse
import numpy as np
import os
import re
import shutil
import glob

import welita
import welita.external.sigproc as sp

def generate(dirs, idexp="^([0-9]+)", ids=None, tablename='demotable', srvpath='', nbsample=4, firstradioid=1,
                outphp=None,            # Output the HTML content in this file.
                open_mode='w',          # Mode of the open function for writing in the PHP file.
                nbgrades=7,             # Set the number of grades to scale the comparison (use 7 for CMOS test and 3 for preference tests)
                randdirs=True,
                nbfirstloop=None,
                randsecondloop=True,
                nbsecondloop=None,
                randswappairs=True,
                player_style='',        # Better to keep it empty for maximum compatibility across browsers ('width: 200px; height: 30px !important;' remove some useless controls though)
                prob_string=u'Prob',
                nowav=False,            # If set, wav files are not copied/exported in the table subdirectory (useful for debugging or updating the table without re-processing all the files).
                wav_resample=None,      # (See sigproc.exportfile(.) )
                wav_highpass_fcut=None, # (See sigproc.exportfile(.) )
                wav_normalize=None,     # (See sigproc.exportfile(.) )
                wav_aligndelay=False,   # (See sigproc.exportfile(.) )
                wav_usepcm16=False      # (See sigproc.exportfile(.) )
                ):

    if outphp is None: outphp=tablename+'.php'       # pragma: no cover
    wavoutdir = os.path.join(os.path.dirname(outphp), tablename)

    print('Generate CMOS/Preference table for listening test: tablename='+tablename)
    print('Deployed at: '+srvpath)
    print('Wav files for this table: '+os.path.join(srvpath, tablename+'/*.wav'))
    for ti, tag in enumerate(dirs):
        print(str(ti)+': '+dirs[ti])

    print('Regular expression for file matching: "'+idexp+'"')

    # Prepare the files
    files = [dict() for k in range(len(dirs))]
    for tn, tag in enumerate(dirs):
        for sndfile in glob.glob(tag+'/*.wav'):
            sndfile = os.path.basename(sndfile)
            rematch = re.search(idexp, sndfile)
            if rematch==None: continue

            idd = rematch.group(1)
            if ids==None or idd in ids:
                files[tn][idd] = sndfile

    # Keep only files that are present in all tests
    commonfiles = files[0]
    for ti in range(1,len(files)):
        commonfiles = set(commonfiles).intersection(files[ti])
    print(str(len(commonfiles))+' files found in common IDs: '+str(sorted(commonfiles)))

    if len(commonfiles)==0:                                 # pragma: no cover
        raise ValueError('There is no samples with common IDs to compare. Please check the content of the directories and the regexp in --idexp argument.')

    sp.makedirs(wavoutdir)

    # Create the wav files
    if not nowav:
        if wav_resample!=None:
            print('WAV: Resample to {}Hz.'.format(wav_resample))
        if wav_highpass_fcut!=None:
            print('WAV: High-pass at {}Hz.'.format(wav_highpass_fcut))
        if wav_normalize!=None:
            print('WAV: Normalize to {}dB.'.format(wav_normalize))
        if wav_aligndelay!=None:
            print('WAV: Align the samples on the samples of the first directory.')
        if wav_usepcm16:
            print('WAV: Rewrite the waveform using pcm16 encoding (16b signed integer).')
        if os.path.isdir(wavoutdir):
            shutil.rmtree(wavoutdir)
        sp.makedirs(wavoutdir)
        for ti in range(len(files)):
            for key in commonfiles:
                print "\rDirectory:{} File:{}                                           ".format(dirs[ti], key),
                sys.stdout.flush()
                fullpath = os.path.join(dirs[ti], files[ti][key])

                destf = os.path.join(wavoutdir, tablename+'.'+str(key)+'.'+str(1+ti)+'.wav')

                if wav_aligndelay and ti>0:
                    sp.exportfile(fullpath, destf, resample=wav_resample, highpass_fcut=wav_highpass_fcut, normalize=wav_normalize, aligndelayref=os.path.join(dirs[0], files[0][key]), usepcm16=wav_usepcm16)
                else:
                    sp.exportfile(fullpath, destf, resample=wav_resample, highpass_fcut=wav_highpass_fcut, normalize=wav_normalize, usepcm16=wav_usepcm16)


                print '\r                                                                                \r',
        #print '\r                                                                                 \n',

    fid = open(outphp,open_mode)

    fid.write('<?php\n')
    fid.write('echo "<table class=\\"test\\">".PHP_EOL;\n')

    fid.write('$radioid='+str(firstradioid)+';\n')
    fid.write('$oriid=1;\n')

    if nbgrades%2==0:
        grades = range(-nbgrades/2,nbgrades/2+1)
        del grades[nbgrades/2]
    else:
        grades = range(-(nbgrades-1)/2,(nbgrades-1)/2+1)
    print('Grades: '+str(grades))
    fid.write('$grades=array();\n')
    for g in grades:
        fid.write('array_push($grades,"'+str(g)+'");\n')
    # fid.write('print_r($grades);\n')

    fid.write('$indices=array();\n')
    for ff in commonfiles:
        fid.write('array_push($indices,"'+str(ff)+'");\n')  # Add the quotes otherwise php might interpret the content, e.g. 0456-02 => 454

    fid.write('shuffle($indices);\n')
    fid.write('$indices = array_slice($indices, 0, '+str(nbsample)+');\n')

    fid.write('foreach ($indices as $index) {\n')
    fid.write('    echo "<tr>";\n')
    fid.write('    echo "<th>Pair</th>";\n')
    fid.write('    echo "<th>File1</th>";\n')
    for g in grades:
        fid.write('    echo "<th>'+(str(g) if g<=0 else '+'+str(g))+'</th>";\n')
    fid.write('    echo "<th>File2</th>";\n')
    fid.write((u'    echo "<th>'+prob_string+u'</th>";\n').encode('utf-8'))
    fid.write('    echo "</tr>".PHP_EOL;\n')

    # TODO That's not actually fully random as the random sequence of the right is repeated for each on the left.
    #      All pairs should be generated first, then randomized vertically and horizontally.

    fid.write('    $dirs = range(1, '+str(len(dirs))+');\n')
    if randdirs:
        fid.write('    shuffle($dirs);\n')
    fid.write('    $ti1s = range(0, '+str(len(dirs)-2)+');\n')

    if not nbfirstloop is None:
        fid.write('    $ti1s = array_slice($ti1s, 0, '+str(nbfirstloop)+');\n')

    fid.write('    foreach ($ti1s as $ti1) {\n')

    fid.write('        $ti2s = range($ti1+1, '+str(len(dirs)-1)+');\n')
    if randsecondloop:
        fid.write('        shuffle($ti2s);\n')

    if not nbsecondloop is None:
        fid.write('        $ti2s = array_slice($ti2s, 0, '+str(nbsecondloop)+');\n')

    fid.write('        foreach ($ti2s as $ti2) {\n')

    fid.write('            echo "<tr>";\n')

    fid.write('            if($radioid%2==1){$paritycolor="odd";}\n')
    fid.write('            else{$paritycolor="even";}\n')

    fid.write('            echo "<td class=\\"$paritycolor\\">$radioid</td>";\n') # The answer id number

    fid.write('            $fileidleft = $dirs[$ti1];\n')
    fid.write('            $fileidright = $dirs[$ti2];\n')

    if randswappairs:
        # Ensure horizontal randomness, no matter what is done before
        fid.write('            if(rand(0,1)==1){\n')
        fid.write('                $fileidtmp=$fileidleft;\n')
        fid.write('                $fileidleft=$fileidright;\n')
        fid.write('                $fileidright=$fileidtmp;\n')
        fid.write('            }\n')

    # Add the sound on the left
    soundfileurl = os.path.join(srvpath, tablename, tablename+'.$index.$fileidleft.wav')
    fid.write('            echo "<td>";\n')
    fid.write('            echo "<audio controls style=\\"'+player_style+'\\" onplay=\\"var d=new Date(); if(getElementById(\'pairtimeleft$radioid\').value.length==0){getElementById(\'pairtimeleft$radioid\').value=d.toISOString()}\\"><source src=\\"'+soundfileurl+'\\" type=\\"audio/wav\\">Your browser does not support the audio element.</audio>";\n')
    fid.write('            echo "</td>";\n')
    #onended=\\"var d=new Date(); getElementById(\'pairtimeleft$radioid\').value=d.toISOString()\\"

    # Put a radio button for each grade
    for g in grades:
        fid.write('            echo "<td class=\\"$paritycolor\\"><input type=radio onclick=\\"var d=new Date(); getElementById(\'pairtime$radioid\').value=d.toISOString()\\" id=pair$radioid name=pair$radioid value=$radioid#$index#$fileidleft#$fileidright#'+str(g)+' /></td>";\n')

    # Add the sound on the right
    soundfileurl = os.path.join(srvpath, tablename, tablename+'.$index.$fileidright.wav')
    fid.write('            echo "<td>";\n')
    fid.write('            echo "<audio controls style=\\"'+player_style+'\\" onplay=\\"var d=new Date(); if(getElementById(\'pairtimeright$radioid\').value.length==0){getElementById(\'pairtimeright$radioid\').value=d.toISOString()}\\"><source src=\\"'+soundfileurl+'\\" type=\\"audio/wav\\">Your browser does not support the audio element.</audio>";\n')
    fid.write('            echo "</td>";\n')

    # Add the "Problem" choice
    fid.write('            echo "<td class=\\"$paritycolor\\"><input type=radio onclick=\\"var d=new Date(); getElementById(\'pairtime$radioid\').value=d.toISOString()\\" name=pair$radioid value=$radioid#$index#$fileidleft#$fileidright#NaN /></td>";\n')

    # Add the invisible time of the last time the listener played the left and right sounds
    fid.write('            echo "<td style=\\"border: 0px;\\"><input type=\\"hidden\\" name=pairtimeleft$radioid id=pairtimeleft$radioid /></td>";\n') #  <td></td>
    fid.write('            echo "<td style=\\"border: 0px;\\"><input type=\\"hidden\\" name=pairtimeright$radioid id=pairtimeright$radioid /></td>";\n')
    # Add the invisible time of the last assessment
    fid.write('            echo "<td style=\\"border: 0px;\\"><input type=\\"hidden\\" name=pairtime$radioid id=pairtime$radioid /></td>";\n')

    fid.write('            $radioid++;\n')

    fid.write('            echo "</tr>".PHP_EOL;\n')

    fid.write('        }\n') # for each method on the right

    fid.write('    }\n') # for each method on the left

    fid.write('}\n') # foreach index

    fid.write('echo "</table>".PHP_EOL;\n')
    fid.write('?>\n')

    fid.close()


def main(argv):
    argpar = argparse.ArgumentParser()
    argpar.add_argument("dirs", nargs='+', help="Directories of wav files corresponding to columns.")
    argpar.add_argument("--ids", nargs='+', default=None, help="IDs to list in the table")
    argpar.add_argument("--idexp", default="^([0-9]+)", help="Regular expression to catch an identificator that match files betwen directories")
    argpar.add_argument("--tablename", default='demotable', help="Name of the table (the table which is included in the test).")
    argpar.add_argument("--firstradioid", default=1, type=int, help="Radio ID of the first line.")
    argpar.add_argument("--srvpath", default='', help="The main server directory where the test is installed.")
    argpar.add_argument("-o", "--outphp", default=None, help="Output the content in the given php file. The waveforms will be saved under the directory of this file.")
    # Test setup
    argpar.add_argument("--nbsample", default=4, type=int, help="Number of samples per listening test.")
    argpar.add_argument("--nbgrades", default=7, type=int, help="Set the number of grades to scale the comparison (use 7 for standard CMOS test, 3 for preference tests, 2 for forced preference test).")
    # Waveform processing
    argpar.add_argument("--wav_resample", default=None, type=int, help="Resample the waveform to the given sampling rate.")
    argpar.add_argument("--wav_highpass_fcut", default=None, type=int, help="High-pass the waveforms at a given frequency (e.g. 50Hz)")
    argpar.add_argument("--wav_normalize", default=None, help="Normalize the waveform to the given dB.")
    argpar.add_argument("--wav_aligndelay", action='store_true', help="Align the samples on the samples of the first directory.")
    argpar.add_argument("--wav_usepcm16", action='store_true', help="Rewrite the waveform using pcm16 encoding (16b signed integer).")
    argpar.add_argument("--nowav", action='store_true', help="If set, wav file are not copied in a subdirectory.")
    args = argpar.parse_args(argv)

    generate(dirs=args.dirs, ids=args.ids, idexp=args.idexp, nowav=args.nowav, tablename=args.tablename, nbsample=args.nbsample, firstradioid=args.firstradioid, srvpath=args.srvpath, outphp=args.outphp, wav_resample=args.wav_resample, wav_highpass_fcut=args.wav_highpass_fcut, wav_normalize=args.wav_normalize, wav_aligndelay=args.wav_aligndelay, wav_usepcm16=args.wav_usepcm16)

if  __name__ == "__main__" :            # pragma: no cover
    main(sys.argv[1:])
