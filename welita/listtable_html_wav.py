#!/usr/bin/env python

'''
COPYRIGHT(C) 2015 Machine Intelligence Lab, Engineering Dept, Univ Cambridge, UK

LICENSE
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
     http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

AUTHOR
    Gilles Degottex <gad27@cam.ac.uk>
'''

import argparse
import numpy as np
import os
import re
import time
import copy

import welita
import welita.external.sigproc as sp

def generate(dirs,                      # A list of directory to pick the audio files from (they will be arranged in columns).
                titles=None,            # Names of the columns (Can be given in the dirs argument as [['dir1', 'method1'], ['dir2', 'method2']].
                idexp="^([0-9]+)",      # Regular expression to catch an identifier that match files betwen the directories.
                ids=None,               # List of specific identifiers to list in the table (comes after the regexp selection of --idexp)
                maxnbline=None,         # Maximum number of lines that will be shown in the table.
                tablename='demotable',  # Name of the table.
                outhtml=None,           # Output the HTML content in this file.
                open_mode='w',          # Mode of the open function for writing in the PHP file.
                player_hide=False,      # Hide the audio player and leave only the download button.
                player_style='',        # Better to keep it empty for maximum compatibility across browsers ('width: 200px; height: 30px !important;' remove some useless controls though)
                keepcommononly=True,    # Keep in the table only the files that are common in all the directories.
                footnotes=False,        # Print some useful information at the end of the table.
                nowav=False,            # If set, wav files are not copied/exported in the table subdirectory (useful for debugging or updating the table without re-processing all the files).
                wav_resample=None,      # (See sigproc.exportfile(.) )
                wav_highpass_fcut=None, # (See sigproc.exportfile(.) )
                wav_normalize=None,     # (See sigproc.exportfile(.) )
                wav_aligndelay=False,   # (See sigproc.exportfile(.) )
                wav_usepcm16=False      # (See sigproc.exportfile(.) )
                ):
    """
    Generate a table of audio files to listen to and ease comparison.

    Note: idexp is assumed to match a single file otherwise sounds might be mixed up in the final comparison table !
            e.g. if using idexp='([0-9]+)\.wav',  samples 949.wav will be mixed with samples 1949.wav

    Example:
        >>> dirswtitles = [
                [datadir+'BDL32_resynthesis', 'Resynth'],
                [datadir+'BDL32_method1', 'MethodA'],
                [datadir+'BDL32_method2', 'MethodB'],
               ]

        The simplest:
        >>> welita.listtable_html_wav.generate(dirswtitles, idexp='arctic_([a|b][0-9]+)', tablename='supersamples')

        A more convenient setup:
        >>> welita.listtable_html_wav.generate(dirswtitles, idexp='arctic_([a|b][0-9]+)', tablename='supersamples', maxnbline=3, footnotes=True, wav_resample=44100, wav_normalize=-26)
    """

    if outhtml is None: outhtml=tablename+'.html'       # pragma: no cover

    dirs = copy.deepcopy(dirs)

    if titles==None and isinstance(dirs[0], list):
        titles = []
        for n in range(len(dirs)):
            titles.append(dirs[n][1])
            dirs[n] = dirs[n][0]
    if titles==None:
        titles = []
        for n in range(len(dirs)): titles.append(str(n))

    print('Generate table with columns: ')
    for ti, tag in enumerate(dirs):
        print(str(ti)+': '+titles[ti]+': '+dirs[ti])
        for forbiddenchar in ('*'):
            if forbiddenchar in titles[ti]: raise ValueError('\''+forbiddenchar+'\' is not a valid character in a column title. This will very likely mess up browsers and any file transfer.')

    print('Regular expression for file matching: "'+idexp+'"')

    # Prepare the files
    files = [dict() for k in range(len(dirs))]
    for tn, tag in enumerate(dirs):
        print('{}: {}: ...'.format(tn, titles[tn])),
        for sndfile in os.listdir(tag):
            m = re.search(idexp, sndfile)
            if m!=None:
                idd = m.group(1)

                if ids==None or idd in ids:
                    if idd in files[tn]: print('WARNING: Multiple files match the regexp in the directory '+tag)
                    files[tn][idd] = sndfile

        print('\b\b\b\b{} files'.format(len(files[tn])))

    if keepcommononly:
        # Find the common keys among the files of all dirs
        comkeys = files[0].keys()
        for fi in xrange(1,len(files)):
            comkeys = list(set(comkeys).intersection(files[fi].keys()))

        for fi in xrange(0,len(files)):
            files[fi] = { comkey: files[fi][comkey] for comkey in comkeys }

    print(str(len(files[0].keys()))+' files found.')
    if len(files[0].keys())==0:
        print('WARNING: No files found. Verify that --idexp matches the files')
        if ids!=None: print('       Some specific ids are given, but there might not be any sound files matching these ids')

    if maxnbline!=None:
        print('Using (limited to first '+str(maxnbline)+' files): '+str(sorted(files[0].keys())[:maxnbline]))
    else:
        print('Using: '+str(sorted(files[0].keys())))

    sp.makedirs(os.path.join(os.path.dirname(outhtml), tablename))

    fid = open(outhtml,open_mode)

    fid.write('<table class="demotable">\n')

    # Write the first head line
    fid.write('<tr>')
    fid.write('<td> </td>') # One empty cell for the line's names (lname)
    for tagi, tag in enumerate(titles):  # Write the coloumn names
        fid.write('<th>'+tag+'</th>')
    fid.write('</tr>\n')
    fid.write('\n')

    # Write one line per "id"
    fss = x = [[] for _ in range(len(files))]
    lasttouchdates = np.zeros(len(files))
    nblines = 0
    for key in sorted(files[0].keys()):
        nblines += 1
        if maxnbline!=None and nblines>maxnbline: continue

        # Get the name of the line
        lname = key

        fid.write('<tr>\n')
        fid.write('<th align="middle"> '+lname+' </th>\n')

        for ti in range(len(files)):
            tooltip = ''
            fid.write('<td align="middle">')
            if key not in files[ti]:
                fid.write('N/A')                            # pragma: no cover
            else:
                fullpath = os.path.join(dirs[ti], files[ti][key])
                lasttouchdates[ti] = max(lasttouchdates[ti], os.path.getmtime(fullpath))

                destf = os.path.join(os.path.dirname(outhtml), tablename, tablename+'.'+str(key)+'.'+titles[ti]+'.wav')

                if not nowav:
                    if wav_aligndelay and ti>0:
                        orifs = sp.exportfile(fullpath, destf, resample=wav_resample, highpass_fcut=wav_highpass_fcut, normalize=wav_normalize, usepcm16=wav_usepcm16, aligndelayref=os.path.join(dirs[0], files[0][key]))
                    else:
                        orifs = sp.exportfile(fullpath, destf, resample=wav_resample, highpass_fcut=wav_highpass_fcut, normalize=wav_normalize, usepcm16=wav_usepcm16)

                    if orifs==None: orifs=sp.wavgetfs(fullpath)
                    fss[ti].append(orifs)

                    tooltip += 'Sampling frequency: '+str(orifs)+'Hz'

                tooltip += '\nLast modification: '+time.strftime("%Y-%m-%d %H:%M",time.localtime(os.path.getmtime(fullpath)))

                soundfileurl = os.path.join(tablename, tablename+'.'+str(key)+'.'+titles[ti]+'.wav')

                fid.write('<table class="invisible"><tr>\n')
                fid.write('<td>\n')

                if not player_hide:
                    fid.write('<audio title="'+tooltip+'" controls style="'+player_style+'"><source src="'+soundfileurl+'" type="audio/wav">Audio element not supported</audio>\n')

                    fid.write('</td><td>\n')

                # Add the download link
                fid.write('<a title="'+tooltip+'" class="dwllink" href="'+soundfileurl+'" rel="nofollow"><img src="dwl.png" alt="dwl" height="16" width="16"></a>\n')

                fid.write('</td>\n')
                fid.write('</tr></table>\n')

            fid.write('</td>\n')

        fid.write('</tr>\n') # Close the line
        fid.write('\n')

    fid.write('</table>\n')

    if footnotes:
        for titlei, title in enumerate(titles):  # Write the coloumn names
            if lasttouchdates[titlei]==0.0:
                touchdate = 'N/A'                   # pragma: no cover
            else:
                touchdate = time.strftime("%Y-%m-%d %H:%M",time.localtime(lasttouchdates[titlei]))
            fid.write('<small>'+title+' [Sampling frequencies: '+str(list(set(fss[titlei])))+' Hz; Last modif.: '+touchdate+']'+': '+dirs[titlei]+'</small><br/>\n')
        if not wav_resample is None:
            fid.write('<small>All files resampled to {}Hz.'.format(wav_resample)+'</small><br/>\n')
        if not wav_normalize is None:
            fid.write('<small>All files normalised to amplitude {}dB'.format(wav_normalize)+'</small><br/>\n')
        if not wav_aligndelay is None:
            fid.write('<small>All files re-aligned to first column files.</small><br/>\n')
        if wav_usepcm16:
            fid.write('<small>All files forced to PCM16 sample format.</small><br/>\n')

    fid.close()

def main(argv):
    argpar = argparse.ArgumentParser()
    argpar.add_argument("directory", nargs='+', help="Directories of wav files corresponding to columns.")
    argpar.add_argument("--titles", nargs='+', default=None, help="Names of the columns (Can be given as directory=[['dir1', 'method1'], ['dir2', 'method2']].")
    argpar.add_argument("--idexp", default="^([0-9]+)", help="Regular expression to catch an identifier that match files betwen directories")
    argpar.add_argument("--ids", nargs='*', default=None, help="List of specific identifiers to list in the table (comes after the regexp selection of --idexp)")
    argpar.add_argument("--tablename", default='demotable', help="Name of the table.")
    argpar.add_argument("--maxnbline", default=None, type=int, help="Use only the first N keys to limit the number of lines in the table.")
    argpar.add_argument("--player_hide", default=False, action='store_true', help="Hide the webplayer (and keep only the download button).")
    argpar.add_argument("--footnotes", action='store_true', help="Add information about the source.")
    argpar.add_argument("-o", "--outhtml", default=None, help="Output the HTML content in this file.")
    # Waveform processing
    argpar.add_argument("--wav_resample", default=None, type=int, help="[Hz] Resample the waveform the given frequency (e.g. 44100Hz).")
    argpar.add_argument("--wav_highpass_fcut", default=None, type=float, help="[Hz] High-pass the waveform according to the given frequency.")
    argpar.add_argument("--wav_normalize", default=None, help="[dB] Normalise the overall file amplitude to the given amplitude (e.g. -26dB)")
    argpar.add_argument("--wav_aligndelay", action='store_true', help="Align temporally the source waveform to the first directory.")
    argpar.add_argument("--wav_usepcm16", action='store_true', help="Save the waveform using PCM16 sample format.")
    argpar.add_argument("--nowav", action='store_true', help="If set, wav file are not copied/exported in the subdirectory (for debugging or updating the table only without re-processing all the files).")
    args = argpar.parse_args(argv)

    generate(dirs=args.directory, titles=args.titles, ids=args.ids, idexp=args.idexp, maxnbline=args.maxnbline, tablename=args.tablename, outhtml=args.outhtml, player_hide=args.player_hide, nowav=args.nowav, footnotes=args.footnotes, wav_resample=args.wav_resample, wav_highpass_fcut=args.wav_highpass_fcut, wav_normalize=args.wav_normalize, wav_aligndelay=args.wav_aligndelay, wav_usepcm16=args.wav_usepcm16)

if  __name__ == "__main__" :            # pragma: no cover
    main(sys.argv[1:])
