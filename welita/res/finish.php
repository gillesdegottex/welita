<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Listening test - Finished</title>
<link href="welita.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div class="finished">

    Thank you for your participation !
    <br/>

    <?php
        include 'log_table_access.php';

        $isamturk = $_GET["hitId"];
//         $isamturk = True;

        // This part is not anonymous-safe and has to ensure that all information sent become anonymous-safe
        $browser = str_replace(';','/', $_SERVER['HTTP_USER_AGENT']);;
        $testId=basename(dirname(__FILE__));
        $startdatetime = $_GET["startdatetime"];
        $submitdatetime = date("Y-m-d H:i:s");
        $startdatetimeobj = new DateTime($startdatetime);
        $submitdatetimeobj = new DateTime($submitdatetime);
        $timetakenobj = $startdatetimeobj->diff($submitdatetimeobj);
        $timetaken = $timetakenobj->format('%H:%i:%s');
        $workerId='anonymous';
        if($isamturk) $workerId=$_GET["workerId"]; # Working on AMTurk
        else          $workerId=$_SERVER["REMOTE_ADDR"]; # Working independently
        $geo = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER["REMOTE_ADDR"]));
        $workercountry = $geo["geoplugin_countryName"];
        $workercity = $geo["geoplugin_city"];
        $assignmentId = $_GET["assignmentId"];

        // Send e-mail notification
        if($_GET["notifmail"]){
            $num = sizeof($_POST);

            //Construct the mail body
            # For anonymity, log the assignmentId, but NOT the $workerId (so that we can discard the data, without knowing the worker)
            $mail_body = 'weblisteningtest;';
            // $mail_body .= ' submitdatetime='.$submitdatetime.';'; // For anonymity, do not add this !
            $mail_body .= ' browser='.$browser.';';
            $mail_body .= ' testId='.$testId.';';
            $mail_body .= ' hitId='.$_GET["hitId"].';';
            $mail_body .= ' assignmentId='.$assignmentId.';';
            $mail_body .= ' workercountry='.$workercountry.';';
            $mail_body .= ' workercity='.$workercity.';';
            $mail_body .= ' timetaken='.$timetaken.';';
            // $mail_body .= ' workerId='.$workerId.'; '; // For anonymity, do not add this !
            $keys = array_keys($_POST);
            $values = array_values($_POST);

            for ($index=0; $index<$num; ++$index) {
                if ($keys[$index]!='submit')
                    $mail_body .= $keys[$index].'='.$values[$index].'; ';
            }

            // Sent the e-mail
            $recipient = $_GET["notifmail"];
            $subject = '[weblisteningtest] '.$testId;
            $header = "From: Listening test <".$recipient.">\r\n"; //optional headerfields
            mail($recipient, $subject, $mail_body, $header);

            // echo 'E-mail sent to: '.$recipient.' '.$subject.': '.$mail_body.'<br/>';
        }

        # Log the participations to the table to avoid duplicates
        # For anonymity, log the workerId, but NOT the assignmentId (otherwise a link would be possible with the submitted data)
        $conn = new mysqli('localhost', $mysqluser, $mysqlpass, $mysqldatabase);
        if ($conn->connect_error) { // Check connection
            echo 'MYSQL: Connection failed: '.$conn->connect_error. '<br/>';
        }
        else {
            $sql = "INSERT INTO $tablename (submitdatetime, testId, hitId, workerId) VALUES ('".$submitdatetime."', '".$testId."', '".$_GET["hitId"]."', '".$workerId."')";
            $result = $conn->query($sql);
            $conn->close();
        }

        // If the test is running on AMTurk ...
        if($isamturk and $_GET["assignmentId"]!="ASSIGNMENT_ID_NOT_AVAILABLE") {
            // ... prepare the hidden form to be sent to AMTurk
            echo '<form name="input" action="'.$_GET["turkSubmitTo"].'/mturk/externalSubmit?assignmentId='.$assignmentId.'&hitId='.$_GET["hitId"].'" method="post">';
            // echo "<input type='hidden' name='submitdatetime' value='".$submitdatetime."'>\n"; For anonymity, do not add this !
            echo "<input type='hidden' name='browser' value='".$browser."'>\n";
            echo "<input type='hidden' name='testId' value='".$testId."'>\n";
            echo "<input type='hidden' name='hitId' value='".$_GET["hitId"]."'>\n";
            echo "<input type='hidden' name='assignmentId' value='".$assignmentId."'>\n";
            echo "<input type='hidden' name='workercountry' value='".$workercountry."'>\n";
            echo "<input type='hidden' name='workercity' value='".$workercity."'>\n";
            foreach ($_POST as $key=>$value)
                echo "<input type='hidden' name='".$key."' value='".$value."'>\n";

            echo '<br/>Sending the results to AMTurk, please wait...';
        }

//         if($isamturk){
//             echo '<br/><br/>If you are happy with this test, please consider making us a review on <a target="_blank" href="https://turkopticon.ucsd.edu/reports?id=TODO">Turkopticon</a>';
//         }
    ?>

    </div>

    <?php
    // If the test is running on AMTurk ...
    if($isamturk and $_GET["assignmentId"]!="ASSIGNMENT_ID_NOT_AVAILABLE")
        echo '<script>setTimeout(function(){document.forms["input"].submit();}, 2000);</script>'; // ... send the hidden form automatically
    ?>

</body>

</html>
