#!/usr/bin/env python

'''
COPYRIGHT(C) 2015 Machine Intelligence Lab, Engineering Dept, Univ Cambridge, UK

LICENSE
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
     http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

AUTHOR
    Gilles Degottex <gad27@cam.ac.uk>
'''

import sys
import argparse
import numpy as np
import os
import re
import shutil
import glob

import welita
import welita.external.sigproc as sp

# TODO The assignment check assumes all dirs are graded!!!
def generate(dirs, idexp='^([0-9]+)', ids=None, tablename=None, srvpath='', nbsample=4, firstradioid=1,
                outphp=None,            # Output the HTML content in this file.
                open_mode='w',          # Mode of the open function for writing in the PHP file.
                mingrade=1, maxgrade=5, noref=False, nohiddenref=False,
                player_style='',        # Better to keep it empty for maximum compatibility across browsers ('width: 200px; height: 30px !important;' remove some useless controls though)
                nowav=False,            # If set, wav files are not copied/exported in the table subdirectory (useful for debugging or updating the table without re-processing all the files).
                wav_resample=None,      # (See sigproc.exportfile(.) )
                wav_highpass_fcut=None, # (See sigproc.exportfile(.) )
                wav_normalize=None,     # (See sigproc.exportfile(.) )
                wav_aligndelay=False,   # (See sigproc.exportfile(.) )
                wav_usepcm16=False      # (See sigproc.exportfile(.) )
                ):

    if outphp is None: outphp=tablename+'.php'                # pragma: no cover
    wavoutdir = os.path.join(os.path.dirname(outphp), tablename)

    print('Generate MOS Table for listening test: tablename='+tablename)
    print('Deployed at: '+srvpath)
    print('Wav files for this table: '+os.path.join(srvpath, tablename+'/*.wav'))
    for ti, dir in enumerate(dirs):
        print(str(ti)+': '+dirs[ti])

    print('Regular expression for file matching: "'+idexp+'"')

    # Prepare the files
    files = [dict() for k in range(len(dirs))]
    for tn, dir in enumerate(dirs):
        for sndfile in glob.glob(dir+'/*.wav'):
            sndfile = os.path.basename(sndfile)
            searchres = re.search(idexp, sndfile)
            if searchres==None: continue

            idd = searchres.group(1)
            if ids==None or idd in ids:
                files[tn][idd] = sndfile


    # Keep only files that are present in all dirs
    commonfiles = files[0]
    for ti in range(1,len(files)):
        commonfiles = set(commonfiles).intersection(files[ti])
    print(str(len(commonfiles))+' files found in common IDs: '+str(sorted(commonfiles)))

    if len(commonfiles)==0:                                   # pragma: no cover
        raise ValueError('There is no samples with common IDs to compare. Please check the content of the directories and the regexp in --idexp argument.')

    sp.makedirs(wavoutdir)

    # Create the wav files
    if not nowav:
        if wav_resample!=None:
            print('WAV: Resample to {}Hz.'.format(wav_resample))
        if wav_highpass_fcut!=None:
            print('WAV: High-pass at {}Hz.'.format(wav_highpass_fcut))
        if wav_normalize!=None:
            print('WAV: Normalize to {}dB.'.format(wav_normalize))
        if wav_aligndelay!=None:
            print('WAV: Align the samples on the samples of the first directory.')
        if wav_usepcm16:
            print('WAV: Rewrite the waveform using pcm16 encoding (16b signed integer).')
        if os.path.isdir(wavoutdir):
            shutil.rmtree(wavoutdir)
        os.makedirs(wavoutdir)
        for ti in range(len(files)):
            for key in commonfiles:
                print "\rDirectory:{} File:{}".format(dirs[ti], key),
                sys.stdout.flush()
                fullpath = os.path.join(dirs[ti], files[ti][key])

                destf = os.path.join(wavoutdir, tablename+'.'+str(key)+'.'+str(1+ti)+'.wav')

                if wav_aligndelay and ti>0:
                    sp.exportfile(fullpath, destf, resample=wav_resample, normalize=wav_normalize, highpass_fcut=wav_highpass_fcut, aligndelayref=os.path.join(dirs[0], files[0][key]), usepcm16=wav_usepcm16)
                else:
                    sp.exportfile(fullpath, destf, resample=wav_resample, normalize=wav_normalize, highpass_fcut=wav_highpass_fcut, usepcm16=wav_usepcm16)

                print '\r                                                                                \r',
        #print '\r                                                                                 \n',

    fid = open(outphp,open_mode)

    fid.write('<?php\n')
    fid.write('echo "<table class=\\"test\\">";\n')

    fid.write('$radioid='+str(firstradioid)+';\n')
    fid.write('$oriid=1;\n')


    fid.write('$indices=array();\n')
    for ff in commonfiles:
        fid.write('array_push($indices,"'+str(ff)+'");\n')

    fid.write('shuffle($indices);\n')
    fid.write('$indices = array_slice($indices, 0, '+str(nbsample)+');\n')

    fid.write('foreach ($indices as $index) {\n')

    fid.write('$dirs = range(1, '+str(len(dirs))+');\n')

    fid.write('echo "<tr style=\'border:0px;\'><td style=\'height:2em; border:0px;\'> </td></tr>";\n')
    fid.write('echo "<tr>";\n')
    fid.write('echo "<th></th>";\n')
    if not noref:
        fid.write('echo "<th>Reference sound:</th>";\n')
        # Add the reference sound
        soundfileurl = os.path.join(srvpath, tablename, tablename+'.$index.$dirs[0].wav') # TODO Hyp: Reference is always the first
        fid.write('echo "<td colspan='+str(maxgrade-mingrade+2)+'>";\n')
        fid.write('echo "<audio controls style=\\"'+player_style+'\\" onplay=\\"var d=new Date(); if(getElementById(\'pairtimeref$radioid\').value.length==0){getElementById(\'pairtimeref$radioid\').value=d.toISOString()}\\"><source src=\\"'+soundfileurl+'\\" type=\\"audio/wav\\">Your browser does not support the audio element.</audio>";\n')
        fid.write('echo "<tr>";\n')
        fid.write('echo "<th></th>";\n')
    fid.write('echo "<th>Sounds to assess</th>";\n')

    if maxgrade==5:
        # maxgrade==5 use standard ITU scale names
        fid.write('echo "<th style=\'width:8em\'>Bad(1)</th><th style=\'width:8em\'>Poor(2)</th><th style=\'width:8em\'>Fair(3)</th><th style=\'width:8em\'>Good(4)</th><th style=\'width:8em\'>Excellent(5)</th><th>Prob</th>";\n')
    else:
        fid.write('echo "<th style=\'width:8em\'>Bad('+str(mingrade)+')</th>')
        for gi in range(mingrade+1,maxgrade):
            fid.write('<th style=\'width:8em\'>('+str(gi)+')</th>')
        fid.write('<th style=\'width:8em\'>Excellent('+str(maxgrade)+')</th><th>Prob</th>";\n')

    # Add the invisible time of the last time the listener played the reference
    fid.write('echo "<td style=\\"border: 0px;\\"><input type=\\"hidden\\" name=pairtimeref$radioid id=pairtimeref$radioid /></td>";\n')

    fid.write('echo "</tr>";\n')

    startdirid = 0
    # If the reference should not be hidden among the samples, skip the reference dir, the first one.
    if not noref and nohiddenref: startdirid = 1

    fid.write('$ti1s = range('+str(startdirid)+', '+str(len(dirs)-1)+');\n')
    fid.write('shuffle($ti1s);\n')
    fid.write('foreach ($ti1s as $ti1) {\n')

    fid.write('echo "<tr>";\n')

    fid.write('if($radioid%2==1){$paritycolor="odd";}\n')
    fid.write('else{$paritycolor="even";}\n')

    fid.write('echo "<td class=\\"$paritycolor\\">$radioid</td>";\n') # The answer id number

    # Add the sound
    soundfileurl = os.path.join(srvpath, tablename, tablename+'.$index.$dirs[$ti1].wav')
    fid.write('echo "<td>";\n')
    fid.write('echo "<audio controls style=\\"'+player_style+'\\" onplay=\\"var d=new Date(); if(getElementById(\'pairtimesnd$radioid\').value.length==0){getElementById(\'pairtimesnd$radioid\').value=d.toISOString()}\\"><source src=\\"'+soundfileurl+'\\" type=\\"audio/wav\\">Your browser does not support the audio element.</audio>";\n')
    fid.write('echo "</td>";\n')
    #onended=\\"var d=new Date(); getElementById(\'pairtimesnd$radioid\').value=d.toISOString()\\"

    # Put a radio button for each grade
    fid.write('$grades = range('+str(mingrade)+', '+str(maxgrade)+');\n')
    fid.write('foreach ($grades as $gi) {\n')
    fid.write('echo "<td class=\\"$paritycolor\\"><input type=radio onclick=\\"var d=new Date(); getElementById(\'pairtime$radioid\').value=d.toISOString()\\" name=pair$radioid value=$radioid#$index#$dirs[$ti1]#".$gi." /></td>";\n')
    fid.write('}\n') # end grades

    # Add the "Problem" choice
    fid.write('echo "<td class=\\"$paritycolor\\"><input type=radio onclick=\\"var d=new Date(); getElementById(\'pairtime$radioid\').value=d.toISOString()\\" name=pair$radioid value=$radioid#$index#$dirs[$ti1]#NaN /></td>";\n')

    # Add the invisible time of the last time the listener played the sound
    fid.write('echo "<td style=\\"border: 0px;\\"><input type=\\"hidden\\" name=pairtimesnd$radioid id=pairtimesnd$radioid /></td>";\n')
    # Add the invisible time of the last assessment
    fid.write('echo "<td style=\\"border: 0px;\\"><input type=\\"hidden\\" name=pairtime$radioid id=pairtime$radioid /></td>";\n')

    fid.write('$radioid++;\n')

    fid.write('echo "</tr>";\n')

    fid.write('}\n') # for each method

    fid.write('}\n') # foreach index

    fid.write('echo "</table>";\n')
    fid.write('?>\n')

    fid.close()

def main(argv):
    argpar = argparse.ArgumentParser()
    argpar.add_argument("dirs", nargs='+', help="Directories of wav files corresponding to columns.")
    argpar.add_argument("--ids", nargs='+', default=None, help="IDs to list in the table")
    argpar.add_argument("--idexp", default="^([0-9]+)", help="Regular expression to catch an identificator that match files betwen directories")
    argpar.add_argument("--tablename", default='demotable', help="Name of the table (the table which is included in the test).")
    argpar.add_argument("--firstradioid", default=1, type=int, help="Radio ID of the first line.")
    argpar.add_argument("--srvpath", default='', help="The main server directory where the test is installed.")
    argpar.add_argument("-o", "--outphp", default=None, help="Output the content in the given php file. The waveforms will be saved under the directory of this file.")
    # Test setup
    argpar.add_argument("--nbsample", default=4, type=int, help="Number of samples per listening test.")
    argpar.add_argument("--mingrade", default=1, type=int, help="Minimum grade.")
    argpar.add_argument("--maxgrade", default=5, type=int, help="Maximum grade.")
    argpar.add_argument("--noref", action='store_true', help="If set, no reference is considered (neither shown as example of each block nor hidden.")
    argpar.add_argument("--nohiddenref", action='store_true', help="If set, the reference is not hidden among the methods.")
    # Waveform processing
    argpar.add_argument("--wav_resample", default=None, type=int, help="Resample the waveform to the given sampling rate.")
    argpar.add_argument("--wav_highpass_fcut", default=None, type=int, help="High-pass the waveforms at a given frequency (e.g. 50Hz)")
    argpar.add_argument("--wav_normalize", default=None, help="Normalize the waveform to the given dB.")
    argpar.add_argument("--wav_aligndelay", action='store_true', help="Align the samples on the samples of the first directory.")
    argpar.add_argument("--wav_usepcm16", action='store_true', help="Rewrite the waveform using pcm16 encoding (16b signed integer).")
    argpar.add_argument("--nowav", action='store_true', help="If set, wav files are not processed and copied in the subdirectories (useful for debugging the test without going through the whole generation)")
    args = argpar.parse_args(argv)

    generate(dirs=args.dirs, ids=args.ids, idexp=args.idexp, tablename=args.tablename, firstradioid=args.firstradioid, srvpath=args.srvpath, outphp=args.outphp,
                nbsample=args.nbsample, mingrade=args.mingrade, maxgrade=args.maxgrade, noref=args.noref, nohiddenref=args.nohiddenref,
                nowav=args.nowav, wav_usepcm16=args.wav_usepcm16, wav_resample=args.wav_resample, wav_highpass_fcut=args.wav_highpass_fcut, wav_normalize=args.wav_normalize, wav_aligndelay=args.wav_aligndelay)

if  __name__ == "__main__" :            # pragma: no cover
    main(sys.argv[1:])
