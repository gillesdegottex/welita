'''
COPYRIGHT(C) 2015 Machine Intelligence Lab, Engineering Dept, Univ Cambridge, UK

LICENSE
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
     http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

AUTHOR
    Gilles Degottex <gad27@cam.ac.uk>
'''

# This version number has to be updated for each release
# (../setup.py reads this file to obtain the version number of the packages)
__version__ = '0.6.1'


import external.sigproc as sigproc

import listtable_html_wav
import listtest_cmos_php_wav
import listtest_mos_php_wav

def makedirs(dir):
    sigproc.makedirs(dir)

def textwrite(demopagefilename, txt, mode='a'):
    fid = open(demopagefilename, mode)
    fid.write(txt)
    fid.close()

def html_header(title, stylesheet='welita.css'):
    title = '''
            <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
            <html lang="en">
                <head>
                    <meta http-equiv="content-type" content="text/html; charset=utf-8">
                    <title>'''+title+'''</title>
                    <link href="'''+stylesheet+'''" rel="stylesheet" type="text/css">
                    <script type="text/javascript" src="script.js">
                    </script>
                    <noscript>
                    <div class="noscriptmsg">
                    <h1>Error</h1>
                    <center>
                    You don't have javascript enabled.<br/>
                    This listening table cannot work without javascript.<br/>
                    Please activate it.
                    </center>
                    </div>
                    </noscript>
                </head>
                <body>
            '''

    return title

def html_footer():
    footer = '''
                </body>
             </html>
             '''

    return footer
