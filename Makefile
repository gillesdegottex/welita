#Copyright(C) 2016 Engineering Department, University of Cambridge, UK.
#
#License
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#     http://www.apache.org/licenses/LICENSE-2.0
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#Author
#   Gilles Degottex <gad27@cam.ac.uk>
#

# Maintenance goals --------------------------------------------------------------

.PHONY: test tests_clean

all: build

welita/external/sigproc:
	git submodule update --init --recursive

build_sigproc: welita/external/sigproc
	cd welita/external/sigproc; make

build: build_sigproc

describe:
	@git describe --tags

clean:

distclean: clean
	cd welita/external/sigproc; $(MAKE) distclean
	find . -name '*.pyc' -delete

# Test goals ------------------------------------------------------------------

test: build
	python -m tests.test_smoke_base
	python -m tests.test_smoke_listtable
	python -m tests.test_smoke_cmos
	python -m tests.test_smoke_mos

testclean:
	rm -fr tests/test_generated_*
