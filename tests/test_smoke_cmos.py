
import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)))
import shutil

import unittest

datadir = 'tests/wav/'

dirs = [
        datadir+'BDL32_resynthesis',
        datadir+'BDL32_method1',
        datadir+'BDL32_method2',
       ]

class TestCMOS(unittest.TestCase):
    def test_smoke_main(self):
        print('TestBase::test_smoke_listtest_cmos_php_wav_main')
        import welita

        outdir = 'tests/test_generated_smoke_main/'

        argv = ['--outphp', outdir+'/index.php', '--idexp', 'arctic_([a|b][0-9]+)']
        argv.extend(dirs)
        print(argv)
        welita.listtest_cmos_php_wav.main(argv)

    def test_smoke_listtest_cmos_php_wav(self):
        print('TestBase::test_smoke_listtest_cmos_php_wav')
        import welita

        outdir = 'tests/test_generated_smoke_listtest_cmos_php_wav/'

        welita.listtest_cmos_php_wav.generate(dirs, idexp='arctic_([a|b][0-9]+)', outphp=outdir+'/index.php', tablename='demotable', nbgrades=2)
        welita.listtest_cmos_php_wav.generate(dirs, idexp='arctic_([a|b][0-9]+)', outphp=outdir+'/index.php', tablename='demotable', nbgrades=3)
        welita.listtest_cmos_php_wav.generate(dirs, idexp='arctic_([a|b][0-9]+)', outphp=outdir+'/index.php', tablename='demotable', nbgrades=4, randdirs=False, nbfirstloop=1, randsecondloop=True)
        welita.listtest_cmos_php_wav.generate(dirs, idexp='arctic_([a|b][0-9]+)', outphp=outdir+'/index.php', tablename='demotable', nbsecondloop=3)
        welita.listtest_cmos_php_wav.generate(dirs, idexp='arctic_([a|b][0-9]+)', outphp=outdir+'/index.php', tablename='demotable', nbgrades=6)
        welita.listtest_cmos_php_wav.generate(dirs, idexp='arctic_([a|b][0-9]+)', outphp=outdir+'/index.php', tablename='demotable', nbgrades=7)
        welita.listtest_cmos_php_wav.generate(dirs, idexp='arctic_([a|b][0-9]+)', outphp=outdir+'/index.php', tablename='demotable', nbgrades=101)
        welita.listtest_cmos_php_wav.generate(dirs, idexp='arctic_([a|b][0-9]+)', outphp=outdir+'/index.php', tablename='demotable')
        welita.listtest_cmos_php_wav.generate(dirs, idexp='arctic_([a|b][0-9]+)', outphp=outdir+'/index.php', tablename='demotable', nbsample=1, firstradioid=17)
        welita.listtest_cmos_php_wav.generate(dirs, idexp='arctic_([a|b][0-9]+)', outphp=outdir+'/index.php', tablename='demotable', srvpath='http://blabla.com/LT/')

        welita.listtest_cmos_php_wav.generate(dirs, idexp='arctic_([a|b][0-9]+)', outphp=outdir+'/index.php', tablename='demotable', wav_resample=44100, wav_highpass_fcut=50, wav_normalize=-32, wav_aligndelay=True, wav_usepcm16=True)

    def test_example_cmos(self):
        print('TestBase::test_example_cmos')
        import welita

        outdir = 'tests/test_generated_example_cmos/'
        welita.makedirs(outdir)
        welita.textwrite(outdir+'/index.php', welita.html_header('Comparative listening test about speech quality (CMOS)'), mode='w')
        welita.listtest_cmos_php_wav.generate(dirs, idexp='arctic_([a|b][0-9]+)', outphp=outdir+'/index.php', tablename='demotable', open_mode='a', nbgrades=7)
        welita.textwrite(outdir+'/index.php', welita.html_footer())

        shutil.copy('welita/res/welita.css', outdir)

    def test_example_preference(self):
        print('TestBase::test_example_preference')
        import welita

        outdir = 'tests/test_generated_example_preference/'
        welita.makedirs(outdir)
        welita.textwrite(outdir+'/index.php', welita.html_header('Preference test about speech quality'), mode='w')
        welita.listtest_cmos_php_wav.generate(dirs, idexp='arctic_([a|b][0-9]+)', outphp=outdir+'/index.php', tablename='demotable', open_mode='a', nbgrades=3)
        welita.textwrite(outdir+'/index.php', welita.html_footer())

        shutil.copy('welita/res/welita.css', outdir)

if __name__ == '__main__':
    unittest.main()
