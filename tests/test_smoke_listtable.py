
import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)))
import shutil

import unittest

datadir = 'tests/wav/'

dirs = [
        datadir+'BDL32_resynthesis',
        datadir+'BDL32_method1',
        datadir+'BDL32_method2',
       ]

dirswtitles = [
        [datadir+'BDL32_resynthesis', 'Resynth'],
        [datadir+'BDL32_method1', 'MethodA'],
        [datadir+'BDL32_method2', 'MethodB'],
       ]


class TestListTable(unittest.TestCase):
    def test_smoke_main(self):
        print('TestBase::test_smoke_listtable_html_wav_main')

        import welita

        argv = ['--outhtml', 'tests/test_generated_smoke_main/demotable.html', '--idexp', 'arctic_([a|b][0-9]+)']
        argv.extend(dirs)
        welita.listtable_html_wav.main(argv)


    def test_smoke_listtable_html_wav(self):
        print('TestBase::test_smoke_listtable_html_wav')

        import welita

        outhtml = 'tests/test_generated_smoke_listtable_html_wav/demotable.html'
        welita.makedirs(os.path.dirname(outhtml))
        welita.listtable_html_wav.generate(dirs, idexp='nimportequoi', outhtml=outhtml)
        welita.listtable_html_wav.generate(dirs, idexp='arctic_([a|b][0-9]+)', outhtml=outhtml)
        welita.listtable_html_wav.generate(dirswtitles, idexp='arctic_([a|b][0-9]+)', outhtml=outhtml, tablename='newdemotable')
        welita.listtable_html_wav.generate(dirswtitles, idexp='arctic_([a|b][0-9]+)', outhtml=outhtml)
        welita.listtable_html_wav.generate(dirswtitles, idexp='arctic_([a|b][0-9]+)', outhtml=outhtml, maxnbline=3)
        welita.listtable_html_wav.generate(dirswtitles, idexp='arctic_([a|b][0-9]+)', outhtml=outhtml, maxnbline=3, footnotes=True)
        welita.listtable_html_wav.generate(dirswtitles, idexp='arctic_([a|b][0-9]+)', outhtml=outhtml, maxnbline=3, footnotes=True, wav_resample=44100)
        welita.listtable_html_wav.generate(dirswtitles, idexp='arctic_([a|b][0-9]+)', outhtml=outhtml, maxnbline=3, footnotes=True, wav_resample=44100, wav_normalize=-26)
        welita.listtable_html_wav.generate(dirswtitles, idexp='arctic_([a|b][0-9]+)', outhtml=outhtml, maxnbline=3, footnotes=True, wav_resample=44100, wav_normalize=-26, wav_aligndelay=True)
        welita.listtable_html_wav.generate(dirswtitles, idexp='arctic_([a|b][0-9]+)', outhtml=outhtml, maxnbline=3, footnotes=True, wav_resample=44100, wav_normalize=-26, wav_aligndelay=True, wav_usepcm16=True)

        shutil.copy('welita/res/dwl.png', os.path.dirname(outhtml))

    def test_example_listtable(self):
        print('TestBase::test_example_listtable')
        import welita

        outhtml = 'tests/test_generated_example_listtable/index.html'
        welita.makedirs(os.path.dirname(outhtml))
        welita.textwrite(outhtml, welita.html_header('Listening table for comparison'), mode='w')
        welita.listtable_html_wav.generate(dirswtitles, idexp='arctic_([a|b][0-9]+)', outhtml=outhtml, open_mode='a', maxnbline=3, footnotes=True, wav_resample=44100, wav_normalize=-26, wav_aligndelay=True, wav_usepcm16=True)
        welita.textwrite(outhtml, welita.html_footer())

        shutil.copy('welita/res/welita.css', os.path.dirname(outhtml))
        shutil.copy('welita/res/dwl.png', os.path.dirname(outhtml))

if __name__ == '__main__':
    unittest.main()
