
import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)))
import shutil

import unittest

datadir = 'tests/wav/'

dirs = [
        datadir+'BDL32_resynthesis',
        datadir+'BDL32_method1',
        datadir+'BDL32_method2',
       ]

class TestMOS(unittest.TestCase):
    def test_smoke_main(self):
        print('TestBase::test_smoke_listtest_mos_php_wav_main')
        import welita

        outdir = 'tests/test_generated_smoke_main/'

        argv = ['--outphp', outdir+'/index.php', '--idexp', 'arctic_([a|b][0-9]+)']
        argv.extend(dirs)
        print(argv)
        welita.listtest_mos_php_wav.main(argv)

    def test_smoke_listtest_cmos_php_wav(self):
        print('TestBase::test_smoke_listtest_mos_php_wav')
        import welita

        outdir = 'tests/test_generated_smoke_listtest_mos_php_wav/'

        welita.listtest_mos_php_wav.generate(dirs, idexp='arctic_([a|b][0-9]+)', outphp=outdir+'/index.php', tablename='demotable')
        welita.listtest_mos_php_wav.generate(dirs, idexp='arctic_([a|b][0-9]+)', outphp=outdir+'/index.php', tablename='demotable', mingrade=0, maxgrade=20)
        welita.listtest_mos_php_wav.generate(dirs, idexp='arctic_([a|b][0-9]+)', outphp=outdir+'/index.php', tablename='demotable', nbsample=1, firstradioid=17)
        welita.listtest_mos_php_wav.generate(dirs, idexp='arctic_([a|b][0-9]+)', outphp=outdir+'/index.php', tablename='demotable', srvpath='http://blabla.com/LT/')

        welita.listtest_mos_php_wav.generate(dirs, idexp='arctic_([a|b][0-9]+)', outphp=outdir+'/index.php', tablename='demotable', wav_resample=44100, wav_highpass_fcut=50, wav_normalize=-32, wav_aligndelay=True, wav_usepcm16=True)

    def test_example_mos(self):
        print('TestBase::test_example_mos')
        import welita

        outdir = 'tests/test_generated_example_mos/'
        welita.makedirs(outdir)
        welita.textwrite(outdir+'/index.php', welita.html_header('Listening test about speech quality (MOS)'), mode='w')
        welita.listtest_mos_php_wav.generate(dirs, idexp='arctic_([a|b][0-9]+)', outphp=outdir+'/index.php', tablename='demotable', open_mode='a')
        welita.textwrite(outdir+'/index.php', welita.html_footer())

        shutil.copy('welita/res/welita.css', outdir)

if __name__ == '__main__':
    unittest.main()
