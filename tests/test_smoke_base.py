
import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)))
import shutil

import unittest

class TestBase(unittest.TestCase):
    def test_smoke_sigproc(self):
        print('TestBase::test_smoke_sigproc')
        import welita

        print(dir(welita.sigproc))

    def test_smoke_makedirs(self):
        print('TestBase::test_smoke_makedirs')
        import welita

        welita.makedirs('tests/test_generated_emptydir')

    def test_smoke_textwrite(self):
        print('TestBase::test_smoke_textwrite')
        import welita

        outhtml = 'tests/test_generated_smoke_init/demotable.html'
        welita.sigproc.makedirs(os.path.dirname(outhtml))
        welita.textwrite(outhtml, '<h1>That is a test!</h1>')
        welita.textwrite(outhtml, '\ndropped!')

    def test_smoke_html_header(self):
        print('TestBase::test_smoke_html_header')
        import welita

        print(welita.html_header('Blabla test'))
        print(welita.html_header('Blabla test', stylesheet='ghoticstyle.css'))

    def test_smoke_html_footer(self):
        print('TestBase::test_smoke_html_footer')
        import welita

        print(welita.html_footer())


if __name__ == '__main__':
    unittest.main()
